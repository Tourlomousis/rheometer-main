This page is served under:
https://tourlomousis.pages.cba.mit.edu/rheometer-main  
and is linked to : https://tourlomousis.pages.cba.mit.edu/rheometer-cover.

# TO DO  
- For printbed
    - design light box (don't forget cut-through slots for cables)
    - laser cut it
    
- For extruder + camera Holder
    - design it 
    - 3DP using PLA
    
- For z-Frame
    - design a U-shaped holder for z-linear actuator (NEAM 17+leadscrew+ x1 linear rail) 
        - take into account syringe+needle tip length
        - start backwords from the light-box
        - design base for z-linear actuator
            - this will mounted on the flat side
            - possibly add an in termediate plate made form Alu
            
    - machine the U-shaped holder
        - Water jet thick HDPE (2") sheet
        - Flat operration using TRAK on the side, where the z-linear actuator will be mounted
        - Holes for mounting the z-linear actuator
        - Holes for mounting on the HDPE base plate  
        
    - 3DP(Eden) base for linear actuator
    
- For extrusion
    - pneumatic
    - make PWM operation of electropneumatic regulator 

- For pressure Measurements
    - figure out the sensor
    
- For computer vision
    - calibrate camera
    
# DONE 
- fix current microscope stage 
    - start with the mini linear axes | miniature flexible shaft couplings form SDP SI
        - http://shop.sdp-si.com/catalog/product/?id=S50MSCMA12H04H04
    - adding linear rails - constraints
        - https://goo.gl/PRt7J5 
    - not side rails but lay them flat to maximize load tolerance
    - add constraint pins to linear rails
    - update CAD model
    - order
- make a new microscope stage using your drawing inspired by the confocal's stage
    - CAD model
    - order

## System Components 
- Uscope Stage:
    - X linear actuator x2:
        - [Miniature Linear Guide + Carriage x2](https://goo.gl/PRt7J5)
    - Y linear actuator x2:
        - [Miniature Linear Guide + Carriage x2](https://goo.gl/PRt7J5)
    - Z linear actuator:
        - [15 mm wide guide rail x2](https://www.mcmaster.com/#6709K33)
        - [Ball Bearing Carriage for 15 mm wide rail x2](https://www.mcmaster.com/#6709K12)